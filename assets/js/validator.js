import { dataService } from '../js/route/middleware/router-service.js';

export const validatorService = {
    'requiered': (value) => !!(value.length === 0),
    'min-length': (value, param) => !!(value.length < param),
    'max-length': (value, param) => !!(value.length > param),
    'point': (value, param) => !!(value.indexOf(param) === -1),
    'email': (value, param) => !!(value.indexOf(param) === -1),
    'check-password': (value) => !checkPass(value),
    'inputRegistration': (inputs) => inputRegistration(inputs),
    'inputLogin': (inputs) => inputLogin(inputs),
    'inputEdit': (inputs) => inputEdit(inputs),
    'checked': () => checkData(),
};

function checkPass(values) {
    let pass = document.querySelector(`input[name=password-checked]`).value;
    return values == pass ? true : false;
}
function checkData() {
    const checkbox = document.querySelector(`#formAgreement`);
    return checkbox.checked === false ? true : false;
}

function inputRegistration(inputs) {
    
    let inputMessage = 'valid';

    inputs.forEach((input) => {
        const value = input.value;

        inputMessage = inputDataValid(input, value, inputMessage);
    });

    return inputMessage === 'valid';
}

async function inputLogin(inputs) {
    const login = inputs[0].value;
    const password = inputs[1].value;

    let isFormValid = false;
    let name = '';

    const usersItems = await dataService.getUsers();

    if (usersItems !== null) {
        usersItems.find((el) => {
            if (el.login === login && el.password === password) {
                isFormValid = true;
                name = el.name;

                inputs[0].value = '';
                inputs[1].value = '';

                return isFormValid;
            } else {
                isFormValid = false;
                return isFormValid;
            }
        });
    }

    return { isFormValid, login, name };
}

function inputEdit(inputs) {
    let inputMessage = 'valid';

    inputs.forEach((item) => {
        if (item.querySelector('input[type=submit]')) return;

        const input = item.querySelector('input') || item.querySelector('select');
        const value = input.value;

        if (value.length === 0) return;

        inputMessage = inputDataValid(input, value, inputMessage);
    });

    return inputMessage === 'valid';
}

function inputDataValid(input, value, inputMessage) {
    if (input.dataset.validators) {
        const validators = [];

        input.dataset.validators.split(',').map((validator) => {
            const validatorStr = validator.trim();
            const name = validatorStr.split('(')[0];
            const param = validatorStr.split(/[()]/)[1];

            validators.push({
                name,
                param,
            });
        });

        validators.forEach((validator) => {
            input.classList.remove(`error--${validator.name}`);

            if (validatorService[validator.name](value, validator.param)) {
                inputMessage = `error--${validator.name}`;
            }
        });
    }

    if (inputMessage !== 'valid') {
        input.classList.add('has-error');
        input.classList.add(inputMessage);
    } else {
        input.classList.remove('has-error');
    }

    return inputMessage;
}
