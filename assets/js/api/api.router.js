export const ROUTER = {
    getUser: async function getUsers(url, param) {
        try {
            const response = await fetch(url, param);
            const res = await response.json();
            return res;
        } catch (error) {
            return false;
        }
    },
    getAllPhotos: async function getAllUserPhotos(url, param) {
        try {
            const response = await fetch(url, param);
            const res = await response.json();
            return res;
        } catch (error) {
            return error;
        }
    },
};
