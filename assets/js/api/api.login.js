export const LOGIN = {
    postLogin: async function postIsLogin(url, param) {
        try {
            const response = await fetch(url, param);
            const res = await response.json();
            return res;
        } catch (error) {
            return '';
        }
    },
    postCreateUser: async function postCreateNewUser(url, param) {
        try {
            const response = await fetch(url, param);
            if (response.status === 201) {
                return true;
            } else {
                throw 'false';
            }
        } catch (error) {
            return false;
        }
    },
};
