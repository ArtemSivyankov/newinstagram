export const USER_PHOTO = {
    deletePhoto: async function deleteUserPhoto(url, param) {
        try {
            const response = await fetch(url, param);
            if (response.status === 204) {
                return true;
            } else {
                throw 'false';
            }
        } catch (error) {
            return false;
        }
    },

    putNewPhoto: async function putNewPhotoUser(url, param) {
        try {
            const response = await fetch(url, param);
            const res = await response.json();
            return res;
        } catch (error) {
            return error;
        }
    },
};
