export const USERS = {
    deleteUser: async function deleteUser(url, param) {
        try {
            const response = await fetch(url, param);
            if (response.status === 204) {
                return true;
            } else {
                throw 'false';
            }
        } catch (error) {
            return false;
        }
    },
    getUser: async function getUser(url, param) {
        try {
            const response = await fetch(url, param);
            const user = await response.json();
            return user;
        } catch (error) {
            return false;
        }
    },
    getAvatar: async function getAvatar(url, param) {
        const response = await fetch(url, param);
        if (!response.ok) throw 'none';
        return response.url;
    },
};
