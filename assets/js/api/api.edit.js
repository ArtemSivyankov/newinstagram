export const EDIT = {
    postEdit: async function postEditPhoto(url, param) {
        const response = await fetch(url, param);
        const urlText = await response.text();
        return urlText;
    },

    postUpdate: async function postUserUpdate(url, param) {
        try {
            const response = await fetch(url, param);
            if (!response.ok) throw new Error('error');
            return response.ok;
        } catch (error) {
            return false;
        }
    },
};
