export const GALLERY = {
    getPhotos: async function getAllPhoto(url, param) {
        const response = await fetch(url, param);
        const res = await response.json();
        return res;
    },

    postLike: async function postLike(url, param) {
        const response = await fetch(url, param);
        const res = await response.text();
        return res;
    },
    getPost: async function getPost(url, param) {
        const response = await fetch(url, param);
        const res = await response.json();
        return res;
    },
    postNewComment: async function postNewComment(url, param) {
        const response = await fetch(url, param);
        const res = await response.json();
        return res;
    },
};
