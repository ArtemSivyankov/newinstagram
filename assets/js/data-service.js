import { GALLERY } from './api/api.gallery.js';
import { LOGIN } from './api/api.login.js';
import { EDIT } from './api/api.edit.js';
import { ROUTER } from './api/api.router.js';
import { USERS } from './api/api.users.js';
import { USER_PHOTO } from './api/api.user-photo.js';

export const DATA_SERVICE = {
    getAllPhoto: (url, param) => GALLERY.getPhotos(url, param),
    postLike: (url, param) => GALLERY.postLike(url, param),
    getPost: (url, param) => GALLERY.getPost(url, param),
    postNewComment: (url, param) => GALLERY.postNewComment(url, param),

    postEditPhoto: (url, param) => EDIT.postEdit(url, param),
    postUserUpdate: (url, param) => EDIT.postUpdate(url, param),

    postIsLogin: (url, param) => LOGIN.postLogin(url, param),
    postCreateNewUser: (url, param) => LOGIN.postCreateUser(url, param),

    deleteUserPhoto: (url, param) => USER_PHOTO.deletePhoto(url, param),
    putNewPhotoUser: (url, param) => USER_PHOTO.putNewPhoto(url, param),

    deleteUser: (url, param) => USERS.deleteUser(url, param),
    getUser: (url, param) => USERS.getUser(url, param),
    getAvatar: (url, param) => USERS.getAvatar(url, param),

    getUsers: (url, param) => ROUTER.getUser(url, param),
    getAllUsersPhotos: (url, param) => ROUTER.getAllPhotos(url, param),
};
