import { Router } from './router.js';

const Route = {
    '': 'gallery.html',
    'gallery': 'gallery.html',
    'users': 'users.html',
    'about': 'about.html',
    'contact': 'contact.html',
    'edit': 'edit.html',
    'user-photos': 'user-photos.html',
};

(() => {
    const router = new Router(Route);
    router.init();
})();
