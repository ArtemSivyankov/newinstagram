import { ROUT_LOAD_CONTROLLER } from './middleware/router-controller.js';
import { dataService } from './middleware/router-service.js';

export class Router {
    constructor(routes) {
        this.routes = routes;
        this.rootElem = document.getElementById('app');
    }

    init() {
        let r = this.routes;

        ((scope, r) => {
            window.addEventListener('hashchange', (e) => {
                scope.hasChanged(scope, r);
            });
        })(this, r);
        this.hasChanged(this, r);
    }

    hasChanged = (scope, r) => {
        const arrHash = window.location.hash.replace('#', '').split('/');
        scope.goToRoute(arrHash, r);
    };

    goToRoute = (arrHash, r) => {
        ((scope) => {
            const hash = arrHash[1] ? r[arrHash[1]] : r[arrHash[0]];
            const url = '/pages/' + hash;

            fetch(url)
                .then((response) => {
                    return response.text();
                })
                .then((data) => {
                    const { id } = dataService.getLogget();
                    const page = scope.rootElem;
                    const hashPage = arrHash[1] ? arrHash[1] : arrHash[0];

                    if (!id) throw new Error(`You are not authorized John Doe`);

                    scope.rootElem.innerHTML = data;

                    ROUT_LOAD_CONTROLLER[hashPage](page.firstChild);

                    window.scrollTo({ top: 0 });
                })
                .catch((err) => {
                    console.log(err);
                });
        })(this);
    };
}
