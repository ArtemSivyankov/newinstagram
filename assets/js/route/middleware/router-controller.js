import { ACTION } from './router-action.js';

export const ROUT_LOAD_CONTROLLER = {
    'gallery': (page) => ACTION.loadGalleryContent(page),
    'users': (page) => ACTION.loadUsersContent(page),
    'edit': (page) => ACTION.loadEdit(page),
    'user-photos': (page) => ACTION.loadUserPhotos(page),
    'contact': (page) => ACTION.loadContactContent(page),
    'about': (page) => ACTION.loadAboutContent(page),
};
