import { DATA_SERVICE } from '../../data-service.js';

export const dataService = {
    getLogget: () => getLogget(),
    getAllPhotos: (page) => getAllPhotos(page),
    getUsers: () => getUsers(),
    getAllUserPhotos: (id) => getAllUserPhotos(id),
    getUser: (id) => getUser(id),
};

function getLogget() {
    const id = JSON.parse(localStorage.getItem('logged'));
    return id;
}

async function getAllPhotos(page) {
    const url = `/load-photos/${page}`;
    const param = { method: 'GET' };

    return DATA_SERVICE.getAllPhoto(url, param);
}

async function getUsers() {
    const url = `/users`;
    const param = { method: 'GET' };

    return DATA_SERVICE.getUsers(url, param);
}

async function getAllUserPhotos(id) {
    const url = `/load-all-photos/${id}`;
    const param = { method: 'GET' };

    return DATA_SERVICE.getAllUsersPhotos(url, param);
}

async function getUser(id) {
    const url = `/user/${id}`;
    const param = { method: 'GET' };

    return DATA_SERVICE.getUser(url, param);
}
