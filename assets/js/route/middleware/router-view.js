export const VIEW_ROUTER = {
    activePage: function activePage() {
        document.querySelector('.header-login').classList.add('hidden');
        document.querySelector('.header-logout').classList.remove('hidden');
        document.querySelector('nav').classList.add('active');
    },
};
