import { dataService } from './router-service.js';
import { VIEW_ROUTER } from './router-view.js';
import { VIEW_GALLERY } from '../../components/gallery/gallery-view.js';
import { VIEW_USERS } from '../../components/users/users-view.js';
import { VIEW_USER_PHOTO } from '../../components/user-photo/user-photo-view.js';
import { VIEW_EDIT } from '../../components/edit/edit-view.js';

export default window.addEventListener('DOMContentLoaded', async () => {
    try {
        const { id } = await dataService.getLogget();
        if (!id) throw new Error('Пользователь не вошел');
        VIEW_ROUTER.activePage();
    } catch (err) {
        console.log(err);
    }
});

export const ACTION = {
    loadGalleryContent: async function loadGalleryContent(page) {
        const photos = await dataService.getAllPhotos(0);
        await photos.forEach((item) => VIEW_GALLERY.drawPhoto(item));
    },

    loadUsersContent: async function loadUsersContent(page) {
        const users = await dataService.getUsers();
        await users.forEach((user) => VIEW_USERS.drawListHtml(user));
    },

    loadUserPhotos: async function loadUserPhotos(page) {
        const id = window.location.hash.split('/user-photos/')[1];

        const arrPhotos = await dataService.getAllUserPhotos(id);
        await arrPhotos.forEach((obj) => VIEW_USER_PHOTO.drawPhoto(obj));
    },

    loadEdit: async function loadEdit(page) {
        const id = window.location.hash.replace('#users/edit/', '');
        const { photo } = await dataService.getUser(id);

        VIEW_EDIT.viewForm(page, photo);
    },

    loadContactContent: async function loadContactContent(page) {
        console.log('load contacts');
    },

    loadAboutContent: function loadAboutContent(page) {
        console.log('load about');
    },
};
