import { DATA_SERVICE } from '../../data-service.js';

export const dataService = {
    deleteUser: (id) => deleteUser(id),
    getUser: (id) => getUser(id),
    getAvatar: (photo, id) => getAvatar(photo, id),
};

async function deleteUser(id) {
    const url = `/user/${id}`;
    const param = {
        method: 'DELETE',
    };

    return DATA_SERVICE.deleteUser(url, param);
}

async function getUser(id) {
    const url = `/user/${id}`;
    const param = {
        method: 'GET',
    };

    return DATA_SERVICE.getUser(url, param);
}
async function getAvatar(photo, id) {
    const url = `${id}/avatar.png`;
    const param = {
        method: 'GET',
    };

    if (photo !== 'none') {
        return DATA_SERVICE.getAvatar(url, param);
    } else {
        return 'none';
    }
}
