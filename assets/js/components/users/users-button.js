import { VIEW_USERS } from './users-view.js';
import { dataService } from './users-service.js';

export default document.addEventListener('click', (e) => clickBtn(e));

function clickBtn(e) {
    const target = e.target;
    const name = target.classList[1];

    if (Object.keys(BUTTON_PRESS).includes(name)) {
        const buttonName = target.classList[1];

        BUTTON_PRESS[buttonName](target);
    }
}

const BUTTON_PRESS = {
    'btn-user-delete': async function (target) {
        VIEW_USERS.deletItem(target);
        await dataService.deleteUser(target.dataset.loginName);
    },

    'open-user-info': function (target) {
        VIEW_USERS.toggleHideModal(target.dataset.modalTarget);
        VIEW_USERS.getUserInfo(target);
    },
};
