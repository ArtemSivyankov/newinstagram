import { dataService } from './users-service.js';

export const VIEW_USERS = {
    getUserInfo: async function getUserInfo(target) {
        const $formInfo = document.querySelector('.form-active');

        const id = target.firstElementChild.dataset.userPhoto;
        const { name, login, phone, date, myCountry, sex, photo } = await dataService.getUser(id);

        $formInfo.innerHTML = `
    
            <div>
                <img class='user-photo-info'
                    alt="User photo"
                    src='${photo !== 'none' ? photo : '/assets/img/icon-user.png'}'>
            </div>
            <div>
                <span><b>Name:</b></span> <span>${name ? name : 'none'}</span>
            </div>
            <div>
                <span><b>Email:</b></span> <span>${login ? login : 'none'}</span>
            </div>
            <div>
                <span><b>Phone:</b></span> <span>${phone ? phone : 'none'}</span>
            </div>
            <div>
                <span><b>Date of Birth:</b></span> <span>${date ? date : 'none'}</span>
            </div>
            <div>
                <span><b>Country:</b></span> <span>${myCountry ? myCountry : 'none'}</span>
            </div>
            <div>
                <span><b>Gender:</b></span> <span>${sex ? sex : 'none'}</span>
            </div>
        `;
    },

    toggleHideModal: function toggleHideModal(dataName) {
        const $idModalOpen = document.querySelector(dataName);
        const $wrappForm = document.querySelector('.wrapp-forms');

        $idModalOpen.classList.toggle('form-active');
        $wrappForm.classList.toggle('active');
    },

    drawListHtml: async function drawListHtml(user) {
        const $lists = document.querySelector('.lists');
        const { _id: id, name, photo } = user;
        const li = document.createElement('li');
        const url = await dataService.getAvatar(photo, id);

        li.classList.add('item');

        li.innerHTML = `
                        <div class='img-user-block' >
                            <button class='btn open-user-info' data-modal-target="#form-user-info">
                                <img class='user-face'
                                    alt='User photo'
                                    data-user-photo='${id}'
                                    src=${url !== 'none' ? url : '/assets/img/icon-user.png'}>
                            </button>
                        </div>
            
                        <div class='text-card'>
                            <span class='header-card'>${name}</span>
                        </div>
            
                        <div>
                            <a href='#users/user-photos/${id}' 
                                class='btn-user-photo' 
                                data-login-name= ${id}>
                                <span class="material-icons md-light md-18">
                                    collections
                                </span>
                            </a>
        
                            <a  href='/#users/edit/${id}' 
                                class='btn-user-edit' 
                                data-login-name= ${id}>
                                  <i class="fa fa-cog" aria-hidden="true"></i>
                            </a>
            
                            <a class='btn btn-user-delete' data-login-name= ${id} >
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>
                        </div>`;

        $lists.appendChild(li);
    },

    deletItem: function deletItem(target) {
        const id = target.dataset.loginName;

        const deleteItemHTML = document.querySelector(`[data-login-name="${id}"]`);

        deleteItemHTML.parentNode.parentNode.classList.add('removeItem');
        setTimeout(() => deleteItemHTML.parentNode.parentNode.remove(), 800);
    },
};
