import { DATA_SERVICE } from '../../data-service.js';

export const dataService = {
    postIsLogin: (user) => postIsLogin(user),
    setLogged: (name) => setLogged(name),
    createdNewUser: (newUser) => createdNewUser(newUser),
};

async function postIsLogin(user) {
    const url = `/login`;
    const param = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user),
    };

    return DATA_SERVICE.postIsLogin(url, param);
}

function setLogged(name) {
    localStorage.setItem('logged', JSON.stringify(name));
}

async function createdNewUser(newUser) {
    const url = `/user`;
    const param = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(newUser),
    };

    return DATA_SERVICE.postCreateNewUser(url, param);
}
