import { dataService } from './login-service.js';

const $formLogin = document.querySelector('#form-login');
const $formReg = document.querySelector('#form-regist');
const $login = document.querySelector('.header-login');
const $logout = document.querySelector('.header-logout');
const $wrappForm = document.querySelector('.wrapp-forms');
const $navMenu = document.querySelector('nav');

export const VIEW_LOGIN = {
    toggleLogForm: function toggleLogForm(event) {
        const target = event.target;

        if (target.className === 'form-reg-page') {
            $formLogin.classList.remove('form-active');
            $formReg.classList.add('form-active');
        } else if (target.className === 'regist-back') {
            $formLogin.classList.add('form-active');
            $formReg.classList.remove('form-active');
        }
    },

    //регистрация
    isDublicateMessageError: function isDublicateMessageError(user, valid) {
        const errMessageDubpicate = document.querySelector('.dublicate-user-err');

        if (!user) {
            valid = true;
            errMessageDubpicate.classList.remove('active');
        } else {
            valid = false;
            errMessageDubpicate.classList.add('active');
        }

        return valid;
    },

    //регистрация
    hideModalLogin: function hideModalLogin(form, valid) {
        if (valid && form === 'formLogin') {
            $formLogin.querySelector('.login-err').classList.remove('active');
            $formLogin.classList.remove('form-active');

            $login.classList.add('hidden');
            $logout.classList.remove('hidden');

            $wrappForm.classList.remove('active');

            $navMenu.classList.add('active');
        } else if (form === 'formReg') {
            $formLogin.classList.add('form-active');
            $formReg.classList.remove('form-active');

            $wrappForm.classList.add('active');
        } else {
            $formLogin.querySelector('.login-err').classList.add('active');
        }
    },

    //header выход
    onClickLogout: function onClickLogout(e) {
        $login.classList.remove('hidden');
        $logout.classList.add('hidden');
        $navMenu.classList.remove('active');

        document.getElementById('app').innerHTML = '';
    },

    closeForm: function closeForm() {
        const $item = document.querySelector('.form-active');

        VIEW_LOGIN.cleareInputs();
        VIEW_LOGIN.toggleHideModal(`#${$item.id}`);
        VIEW_LOGIN.clearHashGalleryPage();
    },

    //модал (регистрация,вход, редакт, удаление)
    toggleHideModal: function toggleHideModal(id) {
        const $idModalOpen = document.querySelector(id);

        $idModalOpen.classList.toggle('form-active');
        $wrappForm.classList.toggle('active');
    },

    //очистка инпутов
    cleareInputs: function cleareInputs() {
        const $item = document.querySelector('.form-active');

        $item.querySelectorAll('input').forEach((input) => {
            if (input.type !== 'color' && input.type !== 'submit') {
                const removeErrorClasses = Array.from(input.classList).slice(1);

                input.classList.remove(...removeErrorClasses);
                input.value = '';
            }
        });
    },

    clearHashGalleryPage: function clearHashGalleryPage() {
        const arrHash = window.location.hash.split('/');

        if (arrHash.includes('#users')) {
            window.location.hash = 'users';
        }
    },

    //показать главную страницу
    isShowMainPage: function isShowMainPage(isUserLogged) {
        if (isUserLogged !== '') {
            window.location.hash = 'gallery';

            dataService.setLogged(isUserLogged);
            VIEW_LOGIN.hideModalLogin('formLogin', true);
        } else {
            VIEW_LOGIN.hideModalLogin('formLogin', false);
        }
    },
};
