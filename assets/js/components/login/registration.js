import { dataService } from './login-service.js';
import { VIEW_LOGIN } from './login-view.js';
import { validatorService } from '../../validator.js';

export default document.addEventListener('click', (e) => clickBtn(e));

function clickBtn(e) {
    const target = e.target;
    const name = target.classList[1];

    if (name === 'item-singup') {
        const dataName = target.dataset.modalTarget;
        VIEW_LOGIN.toggleHideModal(dataName);
    }
}

const $formReg = document.querySelector('#form-regist');

$formReg.addEventListener('submit', submitRegForm);
$formReg.addEventListener('click', VIEW_LOGIN.toggleLogForm);

async function submitRegForm(e) {
    e.preventDefault();
    const $inputs = $formReg.querySelectorAll('.form-input');

    const isFormValid = await validatorService.inputRegistration($inputs);
    const isUserCreated = await createNewUser($inputs, isFormValid);

    if (isFormValid && isUserCreated) {
        VIEW_LOGIN.hideModalLogin('formReg', isFormValid);
    }
}

async function createNewUser($inputs, isFormValid) {
    if (!isFormValid) return false;
    else {
        const newUs = {};
        $inputs.forEach((input) => (newUs[input.name] = input.value));

        const createdStatus = await dataService.createdNewUser(newUs);
        return createdStatus;
    }
}
