import { dataService } from './login-service.js';
import { VIEW_LOGIN } from './login-view.js';

export default document.addEventListener('click', (e) => clickLoginBtn(e));

function clickLoginBtn(e) {
    const target = e.target;
    const name = target.classList[1];

    if (name === 'item-login') {
        const dataName = target.dataset.modalTarget;

        VIEW_LOGIN.toggleHideModal(dataName);
        VIEW_LOGIN.cleareInputs();
    }
}

const $formLogin = document.querySelector('#form-login');

$formLogin.addEventListener('submit', submitLoginForm);
$formLogin.addEventListener('click', VIEW_LOGIN.toggleLogForm);

async function submitLoginForm(e) {
    e.preventDefault();
    const $inputs = $formLogin.querySelectorAll('.form-input');

    const user = {
        login: $inputs[0].value,
        password: $inputs[1].value,
    };

    const isUserLogged = await dataService.postIsLogin(user);

    VIEW_LOGIN.isShowMainPage(isUserLogged);
}
