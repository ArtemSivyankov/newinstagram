import { dataService } from './login-service.js';
import { VIEW_LOGIN } from './login-view.js';

export default document.addEventListener('click', (e) => clickBtn(e));

function clickBtn(e) {
    const target = e.target;
    const name = target.classList[1];

    if (Object.keys(BUTTON_PRESS).includes(name)) {
        const buttonName = target.classList[1];

        BUTTON_PRESS[buttonName](target);
    }
}

const BUTTON_PRESS = {
    'item-logout': function () {
        VIEW_LOGIN.onClickLogout();
        dataService.setLogged({ id: '' });
    },
    'btn-close': function () {
        VIEW_LOGIN.closeForm();
    },

    'wrapp-forms': function () {
        VIEW_LOGIN.closeForm();
    },
};
