import { dataService } from './gallery-service.js';
import { VIEW_GALLERY } from './gallery-view.js';

let page = 1;

document.addEventListener('DOMContentLoaded', () => {
    window.addEventListener('hashchange', () => {
        page = 0;
    });

    document.addEventListener('click', async (e) => {
        const target = e.target;

        if (target.classList.contains('load-more')) {
            const photos = await dataService.getAllPhotos(page);
            if (photos.length < 10) target.remove();

            photos.forEach((item) => VIEW_GALLERY.drawPhoto(item));
            page++;
        }
    });
});
