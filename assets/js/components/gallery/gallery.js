import { dataService } from './gallery-service.js';
import { VIEW_GALLERY } from './gallery-view.js';

export default document.addEventListener('click', async (e) => {
    const target = e.target;

    if (target.classList.contains('btn-likes')) {
        const obj = {
            imgId: target.dataset.imgId,
            userId: JSON.parse(localStorage.getItem('logged')).id,
        };

        dataService.editLikes(obj);
        VIEW_GALLERY.editNumberLikes(target);
    } else if (target.classList.contains('btn-comment')) {
        const imgId = target.dataset.imgId;
        const { photo, comment } = await dataService.getPost(imgId);

        VIEW_GALLERY.showPopapComment(photo, comment);
    } else if (target.classList.contains('btn-close')) {
        VIEW_GALLERY.closePopapComment();
    } else if (target.classList.contains('sent-comment')) {
        const $textComment = document.querySelector('.form-sent-comment .text');
        const idImg = document.querySelector('[data-id-img]').getAttribute('data-id-img');

        const obj = {
            comment: $textComment.value,
            user: JSON.parse(localStorage.getItem('logged')).id,
            img: idImg,
        };

        const req = await dataService.postNewComment(obj);
        VIEW_GALLERY.addNewComment(req);
    }
});
