export const VIEW_GALLERY = {
    drawPhoto: async function drawPhoto({ path, createdAt, nameUser, text, _id, likes }) {
        const $lists = document.querySelector('.lists');

        const today = new Date(createdAt);
        const month = today.toString().split(' ')[1];
        const day = today.getDate();
        const hour = today.getHours();
        const minute = today.getMinutes() < 10 ? '0' + today.getMinutes() : today.getMinutes();

        const userLogged = JSON.parse(localStorage.getItem('logged'));
        const isLike = likes.some((obj) => obj.user._id === userLogged.id);

        const li = document.createElement('li');
        li.classList.add('item');

        const arrLikes = likes.map((obj) => `<img src='${obj.user.photo}'>`);

        li.innerHTML = `
                <img style="background-image: url(${path})" loading="lazy"'> 
                    
                <div class='title-text'>
                    <span class='name-user'>${nameUser}</span>
                    <span class='date-publication'> ${day} ${month}, at ${hour}:${minute}</span>
                </div> 
    
                <button type='button' 
                        class="btn-likes ${isLike ? 'img-like' : ''}" 
                        data-img-id="${_id}" 
                        >

                    <div class=users-likes data-title="${likes.length}">
                        ${arrLikes.join('')}  
                    </div>

                </button>

                <button type='button' data-img-id='${_id}' class="btn-comment"></button>
       
                <div class='comment-text'>
                    <span>${text}</span>
                </div>`;

        $lists.append(li);
    },

    editNumberLikes: function editNumberLikes(target) {
        let elem = target.firstElementChild.dataset.title;
        target.classList.toggle('img-like');

        if (target.classList.contains('img-like')) {
            const count = Number.parseInt(elem) + 1;
            target.firstElementChild.dataset.title = count;

            const img = document.createElement('img');
            img.src = JSON.parse(localStorage.getItem('logged')).photo;
            target.firstElementChild.prepend(img);
        } else {
            const count = Number.parseInt(elem) - 1;
            target.firstElementChild.dataset.title = count;
            target.firstElementChild.firstElementChild.remove();
        }
    },

    showPopapComment: function showPopapComment({ _id, path }, comment) {
        const $app = document.querySelector('#app');

        const wrap = document.createElement('div');
        wrap.classList.add('wrap-comment');
        $app.append(wrap);

        const arrComment = comment.map((obj) => {
            return `<div>
                        <h4 class='user-name'>${obj.user.name}</h4>
                        <img class='user-ava' src='${obj.user.photo}'>
                        <span class='text-comment'>${obj.text}</span>
                        <span class='data-comment'>${setTime(obj.createdAt)}</span>
                     </div>`;
        });

        wrap.innerHTML = `
        <input type='button' class='btn-close'/>
        <div class='popap-comment' data-id-img='${_id}'>

            <img src="${path}" >
        
            <div class='content-comment'>

                <h2>Comments for photo</h2>

                <div class='all-comments'>${arrComment.join('')}</div>

                <form class='form-sent-comment'>
                    <input class='text' type='text'></input>
                    <input class="btn sent-comment" type="button" value="Sent">
                </fomt>
            </div>
        </div>
        `;

        document.querySelector('.all-comments').lastChild?.scrollIntoView(false);
    },
    closePopapComment: function closePopapComment() {
        document.querySelector('.wrap-comment').remove();
    },

    addNewComment: function addNewComment(obj) {
        const $textComment = document.querySelector('.form-sent-comment .text');
        const $containComments = document.querySelector('.all-comments');
        const div = document.createElement('div');

        div.innerHTML = `
            <h4 class='user-name'>${obj.user.name}</h4>
            <img class='user-ava' src='${obj.user.photo}'>
            <span class='text-comment'>${obj.text}</span>
            <span class='data-comment'>${setTime(obj.createdAt)}</span>
        `;

        $containComments.append(div);
        $textComment.value = '';
        document.querySelector('.all-comments').lastChild?.scrollIntoView(false);
    },
};

function setTime(time) {
    let ms = Math.round((Date.now() - new Date(time).getTime()) / 1000);

    if (ms / 3600 >= 24) {
        const day = Math.round(ms / 86400) + ' day ago';
        return day;
    } else if (ms / 60 >= 60) {
        const hour = Math.round(ms / 3600) + ' hour ago';
        return hour;
    } else if (ms >= 60) {
        const minute = Math.round(ms / 60) + ' min ago';
        return minute;
    } else {
        return ms + ' sec';
    }
}
