import { DATA_SERVICE } from '../../data-service.js';

export const dataService = {
    getAllPhotos: (page) => getAllPhotos(page),
    editLikes: (obj) => editLikes(obj),
    getPost: (id) => getPost(id),
    postNewComment: (obj) => postNewComment(obj),
};

async function getAllPhotos(page) {
    const url = `/load-photos/${page}`;
    const param = { method: 'GET' };

    return DATA_SERVICE.getAllPhoto(url, param);
}

async function editLikes(obj) {
    const url = '/gallery/likes';
    const param = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(obj),
    };

    return DATA_SERVICE.postLike(url, param);
}

async function getPost(id) {
    const url = `/gallery/post/${id}`;
    const param = {
        method: 'get',
    };

    return DATA_SERVICE.getPost(url, param);
}

async function postNewComment(obj) {
    const url = '/photo/add-comment';
    const param = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(obj),
    };

    return DATA_SERVICE.postNewComment(url, param);
}
