import { DATA_SERVICE } from '../../data-service.js';

export const dataService = {
    postUserUpdate: (id, userObj) => postUserUpdate(id, userObj),
    postEditPhoto: (id) => postEditPhoto(id),
};

async function postEditPhoto(id) {
    const $form = document.querySelector('#form-edit.form-active');
    const newForm = new FormData($form);

    const url = `/upload/${id}`;
    const param = {
        method: 'POST',
        body: newForm,
    };

    return DATA_SERVICE.postEditPhoto(url, param);
}

async function postUserUpdate(id, userObj) {
    const url = `/user/${id}`;
    const param = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(userObj),
    };

    return DATA_SERVICE.postUserUpdate(url, param);
}
