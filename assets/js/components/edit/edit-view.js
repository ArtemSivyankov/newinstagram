const $wrappForm = document.querySelector('.wrapp-forms');

export const VIEW_EDIT = {
    toggleHideModal: function toggleHideModal(dataName) {
        const $idModalOpen = document.querySelector(dataName);
        
        cleareInputs(document.querySelector('.form-active'));

        $idModalOpen.classList.toggle('form-active');
        $wrappForm.classList.toggle('active');

        window.location.hash = 'users';
    },

    viewForm: function viewForm(page, photo) {
        document.querySelector('.wrapp-forms').classList.add('active');
        page.classList.add('form-active');

        const img = photo !== 'none' ? photo : './assets/img/person_add_black_24dp.svg';
        page.querySelector('.photo-file').style.backgroundImage = `url('${img}')`;
    },

    idShowError: function idShowError(isValid) {
        if (!isValid) {
            let inputErr = document.getElementById('edit-email');
            inputErr.classList.add('has-error', 'error--mail-match');
        } else {
            let inputErr = document.getElementById('edit-email');
            inputErr.classList.remove('has-error', 'error--mail-match');
        }
    },
};

function cleareInputs(inputs) {
    inputs.querySelectorAll('input').forEach((input) => {
        if (input.type !== 'color' && input.type !== 'submit') {
            const removeErrorClasses = Array.from(input.classList).slice(1);

            input.classList.remove(...removeErrorClasses);
            input.value = '';
        }
    });
}
