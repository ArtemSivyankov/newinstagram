import { dataService } from './edit-service.js';
import { validatorService } from '../../validator.js';
import { VIEW_EDIT } from './edit-view.js';

export default document.addEventListener('click', (e) => clickSaveEdit(e));

async function clickSaveEdit(e) {
    const target = e.target;

    if (target.classList.contains('btn-save')) bntSaveEdit();
}

async function bntSaveEdit() {
    event.preventDefault();

    const id = window.location.hash.split('/')[2];
    const isUserUpdate = await saveEditUser(id);

    if (isUserUpdate) VIEW_EDIT.toggleHideModal('#form-edit');
}

async function saveEditUser(id) {
    event.preventDefault();

    const $formEdit = document.querySelectorAll('#form-edit.form-active .form-item');
    const isValid = validatorService.inputEdit($formEdit);

    console.log('VALID', isValid);

    if (isValid) {
        const userObj = {};

        $formEdit.forEach((item) => {
            const input = item.querySelector('input') || item.querySelector('select');

            if (input.value.trim().length === 0) return;
            else userObj[input.name] = input.value;
        });

        userObj.photo = await dataService.postEditPhoto(id);
        let isValid = await dataService.postUserUpdate(id, userObj);
        VIEW_EDIT.idShowError(isValid);

        return isValid;
    }
}
