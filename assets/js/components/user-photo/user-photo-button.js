import { BUTTONS_USER } from './user-photo-action.js';

export default document.addEventListener('click', (e) => clickBtn(e));

function clickBtn(e) {
    const target = e.target;
    const name = target.classList[1];

    if (Object.keys(BUTTONS_USER).includes(name)) {
        const buttonName = target.classList[1];

        BUTTONS_USER[buttonName](target);
    }
}
