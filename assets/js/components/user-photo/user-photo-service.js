import { DATA_SERVICE } from '../../data-service.js';

export const dataService = {
    deletImg: (id) => deletImg(id),
    putNewPhoto: () => putNewPhoto(),
};

async function deletImg(id) {
    const url = `/img/${id}`;
    const param = { method: 'DELETE' };

    return DATA_SERVICE.deleteUserPhoto(url, param);
}

async function putNewPhoto() {
    const id = window.location.hash.split('/user-photos/')[1];
    const value = document.querySelector('textarea[name=text]').value;
    const $form = document.querySelector('#form-add-photo');
    const newForm = new FormData($form);

    const url = `/upload-new-photo/${id}/${value}`;
    const param = {
        method: 'POST',
        body: newForm,
    };

    return DATA_SERVICE.putNewPhotoUser(url, param);
}
