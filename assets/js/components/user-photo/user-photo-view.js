import { dataService } from './user-photo-service.js';

export const VIEW_USER_PHOTO = {
    openModalDialogDeteled: function openModalDialogDeteled() {
        const $content = document.querySelector('.content');

        const div = document.createElement('div');
        div.classList.add('wrapp-dialog');

        div.innerHTML = `
        <div class='modal_dialog'>
            <div class='title-modal'>
                You are sure?
            </div>
    
            <div class='button-modal'>
                <button class='btn-delete-agree'>Yes</button>
                <button class='btn-delete-not'>No</button>
            </div>
        </div>;
        `;

        $content.append(div);

        return div;
    },

    drawPhoto: function drawPhoto({ path, _id }) {
        const $lists = document.querySelector('.lists');

        const li = document.createElement('li');
        li.classList.add('item');

        li.innerHTML = `
                <button class='btn delete-item-img' type='button' data-id-img='${_id}'>
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                </button>
    
                <img style="background-image: url(${path})" loading="lazy"'> 
        `;
        $lists.prepend(li);

        document.querySelector('.wrapp-dialog')?.remove();
    },

    openModaladdedPhoto: function openModaladdedPhoto(src) {
        const $content = document.querySelector('.content');

        const div = document.createElement('div');
        div.classList.add('wrapp-dialog');

        div.innerHTML = `
        <div class='modal_dialog_prev'>

            <button class='btn close_modal_photo'>
                <span class="material-icons">
                close
                </span>
            </button>

           <img class='img-preview' src=${src}>

           <textarea name="text" maxlength ="100">Comment text</textarea>

           <button class='send-photo'>SEND</button>
        </div>
        `;

        $content.append(div);

        return div;
    },

    isRemoveElement: async function isRemoveElement(e, id) {
        if (e.target.classList.contains('btn-delete-agree')) {
            await dataService.deletImg(id);

            document.querySelector(`[data-id-img='${id}']`).parentElement.remove();
            document.querySelector('.wrapp-dialog').remove();
        } else {
            document.querySelector('.wrapp-dialog').remove();
        }
    },
};
