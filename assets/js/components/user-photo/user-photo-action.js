import { dataService } from './user-photo-service.js';
import { VIEW_USER_PHOTO } from './user-photo-view.js';

export const BUTTONS_USER = {
    'photo-file': async function () {
        const $file = document.getElementById('photo');

        $file.addEventListener('change', async function fileChange(e) {
            if (e.target.value) {
                let file = e.target.files[0];
                let reader = new FileReader();

                reader.readAsDataURL(file);

                reader.onload = async function () {
                    btnChangeFile(reader, e);
                };
            }

            $file.removeEventListener('change', fileChange);
        });
    },

    'delete-item-img': function (event) {
        const divModal = VIEW_USER_PHOTO.openModalDialogDeteled();
        const id = event.dataset.idImg;

        divModal.addEventListener('click', (e) => VIEW_USER_PHOTO.isRemoveElement(e, id));
    },
};

const btnChangeFile = (reader, e) => {
    const divForm = VIEW_USER_PHOTO.openModaladdedPhoto(reader.result);

    divForm.addEventListener('click', async (event) => {
        if (event.target.classList.contains('send-photo')) {
            const obj = await dataService.putNewPhoto();

            VIEW_USER_PHOTO.drawPhoto(obj);

            e.target.value = '';
        } else if (event.target.classList.contains('close_modal_photo')) {
            document.querySelector('.wrapp-dialog').remove();
            e.target.value = '';
        }
    });
};
